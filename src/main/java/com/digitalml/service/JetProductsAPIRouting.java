package com.digitalml.service;

import static spark.Spark.*;
import spark.*;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;

import static net.logstash.logback.argument.StructuredArguments.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;

public class JetProductsAPIRouting {

    private static final Logger logger = LoggerFactory.getLogger("jetproductsapi:1.0");

    public static void main(String[] args) {
    
        port(4567);
    
        get("/ping", (req, res) -> {
            return "pong";
        });
        
        get("/halt", (request, response) -> {
			stop();
			response.status(202);
			return "";
		});
		
        // Handle timings
        
        Map<Object, Long> timings = new ConcurrentHashMap<>();
        
        before(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		timings.put(request, System.nanoTime());
        	}
        });
        
        after(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		long start = timings.remove(request);
        		long end =  System.nanoTime();
        		logger.info("log message {} {} {} {} ns", value("apiname", "jetproductsapi"), value("apiversion", "1.0"), value("apipath", request.pathInfo()), value("response-timing", (end-start)));
        	}
        });
        
        afterAfter(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		timings.remove(request);
        	}
        });

        put("/merchant-skus/:user_defined_sku_id/price", (req, res) -> {
        
            StringBuffer sb = new StringBuffer();

            sb.append("Request = " + req.queryParams("Request"));

            return "Price Uploads " + sb.toString();
        });
        put("/merchant-skus/:user_defined_sku_id/Inventory", (req, res) -> {
        
            StringBuffer sb = new StringBuffer();

            sb.append("Request = " + req.queryParams("Request"));

            return "Inventory Upload " + sb.toString();
        });
        put("/merchant-skus/:user_defined_sku_id/variation", (req, res) -> {
        
            StringBuffer sb = new StringBuffer();

            sb.append("Request = " + req.queryParams("Request"));

            return "Variation Upload " + sb.toString();
        });
        put("/merchant-skus/:user_defined_sku_id", (req, res) -> {
        
            StringBuffer sb = new StringBuffer();

            sb.append("Request = " + req.queryParams("Request"));

            return "SKU Upload " + sb.toString();
        });
        put("/merchant-skus/:user_defined_sku_id/shippingexception", (req, res) -> {
        
            StringBuffer sb = new StringBuffer();

            sb.append("Request = " + req.queryParams("Request"));

            return "Shipping Exception Upload " + sb.toString();
        });
        put("/merchant-skus/:user_defined_sku_id/returnsexception", (req, res) -> {
        
            StringBuffer sb = new StringBuffer();

            sb.append("Request = " + req.queryParams("Request"));

            return "Returns Exception " + sb.toString();
        });
        put("/merchant-skus/:user_defined_sku_id/status/archive", (req, res) -> {
        
            StringBuffer sb = new StringBuffer();

            sb.append("Request = " + req.queryParams("Request"));

            return "Archive SKU " + sb.toString();
        });
        get("/merchant-skus/:user_defined_sku_id", (req, res) -> {
        
            StringBuffer sb = new StringBuffer();
            sb.append("user_defined_sku_id = " + req.params("user_defined_sku_id"));


            return "Single SKU Retrieval " + sb.toString();
        });
        get("/merchant-skus/:user_defined_sku_id/price", (req, res) -> {
        
            StringBuffer sb = new StringBuffer();
            sb.append("user_defined_sku_id = " + req.params("user_defined_sku_id"));


            return "Price Retrieval " + sb.toString();
        });
        get("/merchant-skus/:user_defined_sku_id/inventory", (req, res) -> {
        
            StringBuffer sb = new StringBuffer();
            sb.append("user_defined_sku_id = " + req.params("user_defined_sku_id"));


            return "Inventory Retrieval " + sb.toString();
        });
        get("/merchant-skus/:user_defined_sku_id/variation", (req, res) -> {
        
            StringBuffer sb = new StringBuffer();
            sb.append("user_defined_sku_id = " + req.params("user_defined_sku_id"));


            return "Variation Retrieval " + sb.toString();
        });
        get("/merchant-skus/:user_defined_sku_id/shippingexception", (req, res) -> {
        
            StringBuffer sb = new StringBuffer();
            sb.append("user_defined_sku_id = " + req.params("user_defined_sku_id"));


            return "Shipping Exception Retrieval " + sb.toString();
        });
        get("/merchant-skus/:user_defined_sku_id/returnsexception", (req, res) -> {
        
            StringBuffer sb = new StringBuffer();
            sb.append("user_defined_sku_id = " + req.params("user_defined_sku_id"));


            return "Returns Exception " + sb.toString();
        });
        get("/merchant-skus", (req, res) -> {
        
            StringBuffer sb = new StringBuffer();


            return "SKU Link Retrieval " + sb.toString();
        });
        get("/merchant-skus/:user_defined_sku_id/salesdata", (req, res) -> {
        
            StringBuffer sb = new StringBuffer();
            sb.append("user_defined_sku_id = " + req.params("user_defined_sku_id"));


            return "SKU Sales Data " + sb.toString();
        });
    }

}